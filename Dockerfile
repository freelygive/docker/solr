FROM solr:7.5

USER root

RUN apt-get update && \
  apt-get -y install git

USER $SOLR_USER

COPY create-ci-core.sh /docker-entrypoint-initdb.d/
