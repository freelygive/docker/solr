#!/bin/bash
set -e

# Check for the required variables.
for VAR in SOLR_CORE_NAME SOLR_CONFIG_DIR SOLR_REPOSITORY_URL SOLR_COMMIT_SHA; do
  if [ -z "${!VAR}" ]; then
    MISSING_VARS="true"
    echo "$VAR is not set"
  fi
done
if [ -n "$MISSING_VARS" ]; then
  exit 1;
fi

# Clone the repo & check out the correct commit.
git clone $SOLR_REPOSITORY_URL /tmp/repo
cd /tmp/repo
git checkout $SOLR_COMMIT_SHA

# Create the core from the config.
/opt/docker-solr/scripts/precreate-core $SOLR_CORE_NAME /tmp/repo/$SOLR_CONFIG_DIR
